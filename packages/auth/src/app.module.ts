import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { config } from './config';
import { userModelSchema } from './models/user';

const { DB_CONNECT_URI } = config;

@Module({
  imports: [
    MongooseModule.forRoot(DB_CONNECT_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }),
    MongooseModule.forFeature([
      { name: 'userModelInjectToken', schema: userModelSchema },
    ]),
  ],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AppModule {
}
