import { Body, Controller, Get, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { config } from './config';

const { API } = config;

@Controller(API)
export class AuthController {
  constructor(private readonly service: AuthService) {
  }

  @Post('register')
  register(@Body() { email, password }: IRegister): Promise<string | void | Error> {
    return this.service.findUser(email);
  }

  @Get()
  getHello(): string {
    return this.service.getHello();
  }
}

interface IRegister {
  email: string;
  password: string;
}
