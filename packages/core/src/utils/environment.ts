export enum Env {
  development = 'development',
  test = 'test',
  production = 'production',
}

export enum AppName {
  AUTH = 'AUTH',
}

const detectEnvironment = (): Env => {
  const env = process.env.NODE_ENV
  return !!env
    ? (Env[env] as Env) || Env.development
    : Env.development
};

export const ENV = detectEnvironment()
