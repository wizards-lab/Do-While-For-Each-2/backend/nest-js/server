import {getLogger} from 'log4js'
import {Response} from 'express'

export class Log {
  private log

  constructor(private level: string,
              private category: string
  ) {
    this.log = getLogger(category)
    this.log.level = level
  }

  error(message: string, error?) {
    this.log.error(message, error && error.message || '')
  }

  info(message: string) {
    this.log.info(message)
  }

  errorThrow(message: string, error?): string {
    this.log.error(message, error && error.message || '')
    throw message
  }

  errorReThrowOrThrow(error, message: string) {
    if (typeof error === 'string') {
      throw error
    } else {
      this.errorThrow(message, error)
    }
  }

  private resError(res: Response, status: number, message: string, error?) {
    this.error(message, error && error.message || '')
    return res.status(status).json({message, detail: error?.message})
  }

  res400(res: Response, message: string, error?) {
    return this.resError(res, 400, message, error)
  }

  res500(res: Response, message: string, error?) {
    return this.resError(res, 500, message, error)
  }
}
