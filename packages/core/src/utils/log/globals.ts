import {ConfigDataType} from '../config'

export const configConsoleInfo = {
  appenders: {
    console: {type: 'console'},
  },
  categories: {
    default: {appenders: ['console'], level: 'info'}
  }
}

export const initModuleInfo = (config: ConfigDataType): string => {
  const {NAME, API, PORT, DB_CONNECT_URI} = config
  return `start ${NAME}[${API}] UP on port ${PORT}`
}
